package AuPProgrammierUebung;

//import java.util.Arrays;

public class A10_permutation {

	public static boolean is_permutation(int[] a) {
		int l=a.length-1;
//		Arrays.sort(a);
		try {
			A9_sort.sort(a);
		} catch (Exception e) {
			return false;
		}
		for (int i = 0; i < l; i++) {
			if(a[i]!=i+1) {
				return false;
			}
		}		
		return true;
	}
	
	public static void main(String[] args) {
		int[] a= {1,3,5,4,2,2};
		System.out.println(is_permutation(a));
	}
	
}
