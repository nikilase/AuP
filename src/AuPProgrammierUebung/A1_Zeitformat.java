package AuPProgrammierUebung;

public class A1_Zeitformat {
	
	public static void format(int input) {
		int tl = 60*60*24;
		int hl = 60*60;
		int ml= 60;
		int t = input/tl;
		int h = (input-t*tl)/hl;
		int m = (input-t*tl-h*hl)/ml;
		int s = (input-t*tl-h*hl-m*ml);
		System.out.println(t+" "+h+" "+m+" "+s);	
	}
	
	public static void main(String[] args) {
		int i = 284712;
		format(i);
	}
}
