package AuPProgrammierUebung;

public class A4_PerfekteZahl {
	
	public static boolean perf(int n) {
		int s=0;
		for(int i =1; i<n;i++) {
			if(n%i==0)
				s+=i;
		}
		return(s==n);
	}
	public static void main(String[] args) {
		int i=28;
		System.out.println(perf(i));
	}
}
