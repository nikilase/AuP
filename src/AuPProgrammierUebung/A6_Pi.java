package AuPProgrammierUebung;

public class A6_Pi {	
	
	public static double pi() {
		double pi=0;
		double last=-100;
		for(int i=1;i<1000;i+=2){	
			for (int j = 1; j <1000; j++) {
				if (j%2==1) {
					last = pi;
					pi = pi + (4.0 / i);
					if (pi - last <= 0.000001)
						i = 10001;
					i+=2;
					
				}
				else if(j%2==0) {
					last = pi;
					pi = pi - (4.0 / i);
					if (last-pi <= 0.000001)
						i = 10001;
					i+=2;
				}
			}
			
		}
		return pi;
	}
	
	public static void main(String[] args) {		
		System.out.println(pi());
	}
}
