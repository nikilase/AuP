package AuPProgrammierUebung;

public class A7_Mittelwert {

	public static double mitte(int[] a) {
		int sum=0;
		for(int i=0; i<a.length;i++) {
			sum+=a[i];
		}
		double m=sum/a.length;
		return m;
	}
	
	public static void main(String[] args) {
		int[] a= {1,3,5,9,4,6,7};
		System.out.println(mitte(a));
	}
}
