package AuPProgrammierUebung;

public class A8_max {

	public static int max(int[] a) {
		return maxRec(a,0,a.length-1);
	}
	
	public static int maxRec(int[] a, int s, int e) {
		int mid=(e-s)/2;
		int max1;
		int max2;
		
		if(a.length==0) {
			return Integer.MIN_VALUE;
		} else if (e==s) {
			return a[s];			
		} else if (e-s==1) {
			if (a[s]>=a[e]) {
				return a[s];
			} else {
				return a[e];
			}
		}
		
		max1=maxRec(a,s,mid);
		max2=maxRec(a,mid+1,e);
		if (max1>=max2) {
			return max1;
		} else {
			return max2;
		}
	}
	
	public static void main(String[] args) {
		int[] a= {-11,5,-8,-1};
		System.out.println(max(a));
	}
}
