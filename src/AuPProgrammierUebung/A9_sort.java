package AuPProgrammierUebung;

public class A9_sort {
	static long startTime;
	
	
	/**
	 * Sorts array of numbers.
	 * Throws exception if there are two identical numbers in the array or the algorithm runs for more than 15 seconds.
	 */
	public static void sort(int[] a) {
		startTime = System.currentTimeMillis();
		quick(a,0,a.length-1);		
	}
	
	public static void quick(int[] a, int l, int r) {
		
		if (l<r) {
			
			int m=partition(a,l,r);
			quick(a,l,m-1);
			quick(a,m+1,r);
		}	
	}
	
	public static int partition(int[] a, int left, int right) {
		int m;
		int p=a[left];
		int l=left;
		int r=right; 
		
		while (l<r) {
			long estimatedTime = System.currentTimeMillis() - startTime;
			if(estimatedTime>=(15*1000)) {
				throw new RuntimeException("Array has multiple same numbers. Please Remove them.");
			}
			while (l<=right&&a[l]<p) {			
				l++;
			}
			while (r>=left&&a[r]>p) {
				
				r--;
			}
			if (l<r) {
				change(a,l,r);
			}
		}
		m=r;
		change(a,l,m);
		return m;
	}
	
	public static void change(int[] a, int l, int r) {
		int h=a[l];
		a[l]=a[r];
		a[r]=h;
	}
	
	public static void main(String[] args) {
		int[] a= {1,3,5,4};
		sort(a);
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

}
