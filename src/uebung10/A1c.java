package uebung10;

public class A1c 
{
	public static void main(String[] args)
	{
		double[] a= {5,1,-6,-4,3,2,1,4};		
		double[] s=maxSubSum(a);
		System.out.println(s[0]+" "+s[1]+" "+s[2]);
		
		
	}
	
	//Hauptprogramm
	public static double[] maxSubSum(double[] a)
	{
		//Laenge und Mitte ermitteln
		double[] s= {-1,-1,-1};
		int n=a.length-1;
		int m=(n)/2;	
		
		//Basis Fall
		if(a.length==1) {
			if(a[0]>=0) {
				s[0]=a[0];
				s[1]=0;
				s[2]=0;
			}else {
				s[0]=0;
			}
			return s;
		}
		//halbieren des Strings
		double[] hl=new double[m+1];
		for(int i=0;i<=m;i++)
			hl[i]=a[i];
		double[] hr=new double[n-m];
		for(int i=m+1;i<=n;i++)
			hr[i-m-1]=a[i];
		//rekurrenzaufruf
		double[] sl=maxSubSum(hl);
		double[] sr=maxSubSum(hr);
		//die richtigen Indizes fuer die rechte seite 
		sr[1]+=m;
		sr[2]+=m;
		
		double sml= sl[0];
		double smr= sr[0];
		//die Summe von links + rechts 
		double[] sm=maxMidSum(a);
		//falls die summe von links + rechts groesser ist als die von links und die von rechts liegt das ergebnis in der links + rechts(Fall 3)
		if(sm[0]>Math.max(sml,smr))
			s=maxMidSum(a);
		else	//sonst liegt das ergebnis entweder links oder rechts
		{
			if(sml>=smr)
				s=sl;
			else {
				s=sr;
			}
		}

		return s;		
	}
	//berechnet die Summe von links+rechts
	
	//berechnet die Indizes und wert von der maximalen Subsumme von links + rechts
	public static double[] maxMidSum(double[] a)
	{
		if(a.length<=2)	
		{
			double t=a[0]+a[1];
			double[]s= {t,0,1};
			return s;
		}
		//suche der indizes der groessten Subsumme nahe mittelpunkt
		int n=a.length-1;
		int m=(n)/2;	
		int sl=m;
		double suml=a[m];
		double sum=a[m];
		//in der linken haelfte von rechts anfangen
		for(int i=m-1;i>=0;i--)	
		{
			suml=suml+a[i];
			if(suml>sum)
				sum=suml;
				sl=i;
		}	
		int er=m+1;
		double sumr=a[m+1];
		//in der rechten haelfte von links anfangen
		sum=a[m+1];
		for(int i=m+2;i<=n;i++)
		{
			sumr=sumr+a[i];			
			if(sumr>sum)
				sum=sumr;
				er=i;
		}	
		//groesste Subsumme errechnen
		sum=0;
		for(int i=sl;i<=er;i++)
			sum=sum+a[i];
		
		double[] s= {sum,sl,er};	
		return s;
	}
	
}
