package uebung10;

public class A1c_2 
	{
    public static void main(String[] args) {
        double[] a = {5,1,-6,4,3,2,1,4};

        double[] maxSubSum = maxSubSum(a);
        System.out.println(maxSubSum[0]);
        System.out.println(maxSubSum[1]);
        System.out.println(maxSubSum[2]);
    }

    public static double[] maxSubSum(double[] a) {
        return maxSubSum(a, 0, a.length-1);
    }

    public static double[] maxSubSum(double[] a, int l, int r) {
        // sub-sequence has 1 element, n = 1
        if (l == r) {
            if (a[l] >= 0) {
                // only element is max sub-sequence
                return new double[]{a[l], l, r};
            }
            else {
                // empty sub-sequence is max sub-sequence with sum 0
                // notated with -1 as indices
                return new double[]{0, -1, -1};
            }
        }
        else {
            int mid = (l+r)/2;

            double[] maxSubSumL = maxSubSum(a, l, mid);
            double[] maxSubSumR = maxSubSum(a, mid+1, r);
            double ml=maxSubSumL[0];
            double mr=maxSubSumR[0];
            // the two sequences are connected
            // maybe add -1 to right side?
            if (midSum(a,l,r)>=Math.max(ml,mr)) {
                double[] maxSubSum = new double[3];
                maxSubSum[0] = maxSubSumL[0] + maxSubSumR[0];
                maxSubSum[1] = maxSubSumL[1];
                maxSubSum[2] = maxSubSumR[2];

                return maxSubSum;
            }
            // the two sequences are not connected -> determine larger one
            else {
                if (maxSubSumL[0] >= maxSubSumR[0]) {
                    return maxSubSumL;
                }
                else {
                    return maxSubSumR;
                }
            }
        }
    }
    public static double midSum(double[] a,int l,int r)
	{
		int n=a.length-1;
		double sum=0;
		for(int i=l;i<=r;i++)
			sum=sum+a[i];
		return sum;
	}
}
