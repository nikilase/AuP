package uebung8;

public class A2 
{
	public static void main(String[] args)
	{
		int F[]= {1,2,3,4,5,6,7,8,9,10};
		int l=F.length;
		for(int i=1; i<=F.length;i++)
			System.out.println(BinSearch(F,i));
	}
	
	public static int BinSearch(int[] F, int k)
	{
		int u=0;	//Start u setzen
		int o=F.length-1;	//Start o setzten
		if(k==F[F.length-1])	//Um Fehler bei suche vom letzten Element zu umgehen
			return F.length-1;
		return BinSearchRec(F,k,u,o);	
	}
	public static int BinSearchRec(int[] F, int k, int u, int o)
	{
		int p =-10;	
		int m=(o-u)/2;	//m ist h�lfte der laenge von u bis o
		if(F[u+m]==k)	
			return(u+m);	//Falls k in Mitte von u,o ist
			else if(F[u+m]>k)	//Falls Mitte groesser als k
			{
				o=u+m;
			}
			else	//sonst
			{
				u=u+m;
			}
		p=BinSearchRec(F,k,u,o);	//Rekurrenzaufruf	
		return(p);
	}
}
