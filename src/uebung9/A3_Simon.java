package uebung9;
public class A3_Simon {
    public static void main(String[] args) {
        int[] S = {2, 3, 5, 6};
        int k = 16;

        int[][] P = knapsack(S, k);
        boolean[] indizes = getIndizes(S, P);
        for(int i=0;i<=indizes.length-1;i++)
        	System.out.print(indizes[i]);
        System.out.println("");
        for(int i=1;1<=P.length;i++) {
			for(int j=1;j<=P[1].length;j++) {
				System.out.print(P[i-1][j-1]+" ");
			}
			System.out.println("");
		}
    }

    public static int[][] knapsack(int[] S, int k) {
        /* Im Algorithmus auf der Folie musste eine Extrazeile eingefuegt werden,
        damit dieser korrekt funktioniert. In dieser Methode wird der Algorithmus
        aus der Folie augerufen und danach diese Extrazeile entfernt.
         */
        int[][] tmp = _knapsack(S, k);
        int[][] P = new int[tmp.length-1][tmp[0].length];

        for (int i = 0; i < P.length; i++) {
            for (int j = 0; j < P[i].length; j++) {
                P[i][j] = tmp[i+1][j];
            }
        }

        return P;
    }

    public static int[][] _knapsack(int[] S, int k) {
        int n = S.length;
        int[][] P = new int[n+1][k+1];
        P[0][0] = 0;

        for (int j = 1; j <= k; j++) {
            P[0][j] = 2;
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= k; j++) {
                P[i][j] = 2;

                if (P[i-1][j] == 0 || P[i-1][j] == 1) {
                    P[i][j] = 0;
                }
                else if ((j - S[i-1] >= 0) && (P[i-1][j-S[i-1]] == 0 || P[i-1][j-S[i-1]] == 1)) {
                    P[i][j] = 1;
                }
            }
        }

        return P;
    }

    public static boolean[] getIndizes(int[] S, int[][] P) 
    {
        boolean[] ind=new boolean[S.length];
        for (int i=P[0].length-1;i>=0;i--) 
        {
            for (int j=0;j<P.length;j++) 
            {
                if(ind[j]!=true&&P[j][i]==1) 
                {
                    ind[j]=true;
                }
            }
        }
        return ind;
    }
}
