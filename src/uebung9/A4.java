package uebung9;

public class A4 {

	public static void main(String[] args)
	{
		int[] S= {1,4,2,3,9,6,7,5};
		int [] mm=mm(S,S.length);
		System.out.println(mm[0]);
		
	}
	
	//mm[0] ist min; mm[1] ist max
	public static int[] mm(int[] s,int l)
	{
		int[] mm= {Integer.MIN_VALUE,Integer.MIN_VALUE};
		if(l==1)	//Vergleich fuer l=1
		{
			mm[0]=s[0];
			mm[1]=s[0];
		}else if(l==2)	//Vergleich fuer l=2
		{
			if(s[0]<=s[1])
			{
				mm[0]=s[0];
				mm[1]=s[1];
			}else {
				mm[0]=s[1];
				mm[1]=s[0];
			}
		}else	//Wenn 2>=2 wird s[] geteilt und mm auf beide haelften angewendet
		{
			int s1[]=new int[l/2];
			int s2[]=new int[l/2];
			for(int i=0;i<l/2;i++)
				s1[i]=s[i];
			for(int i=0;i<l/2;i++)
				s2[i]=s[i+l/2];
			
			int[] mm1=mm(s1,l/2);
			int[] mm2=mm(s2,l/2);
							
			if(mm1[0]<=mm2[0])	//Am ende werden noch die mins und maxs verglichen um den endgueltigen min max zu finden
				mm[0]=mm1[0];
			else mm[0]=mm2[0];

			if(mm1[1]>=mm2[1])
				mm[1]=mm1[1];
			else mm[1]=mm2[1];					
		}		
		return mm;
	}

}
