package �bung2;

public class A3 
{
	public static void main(String[] args) 
	{
		int max=0;
		int a[]= {13,55,88,-12,-99,101,-200,300};

		max=forSchleife(a);
		System.out.println("For-Schleife"+max);

		max=doSchleife(a);
		System.out.println("Do-Schleife"+max);

		max=whileSchleife(a);
		System.out.println("While-Schleife"+max);
	}	
	private static int forSchleife(int[] a)
	{
		int m=a[0];
		
		for(int i=a.length-1;i>=0; i--) 
		{	
			if(a[i]>m) m=a[i];
		}
		return(m);
	}
	private static int doSchleife(int[] a)
	{
		int m=a[0];
		int i=0;
		do 
		{
			if(a[i]>m) m=a[i];
			i++;
		}
		while(i<a.length);
		return(m);
	}
	private static int whileSchleife(int[] a)
	{
		int m=a[0];
		int i=0;
		while(i<a.length) 
		{	
			if(m<a[i]) m=a[i];
			i++;
		}
		return(m);
	}	
}