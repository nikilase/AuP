package �bung3;

import java.util.Random;

public class A1a 
{
	public static void main(String[] args)
	{
		int height=15;	//H�he des Felds
		int width=15; 	//Breite des Felds
		int[][] a=field(height,width);	//Neues random Feld erstellen
		int counter=0;	//Counter f�r die Anzahl der Runden
        while(living(a)>0) //Hauptschleife
        {
        	counter++;	
        	utf8P(a);	//Ausgabe des Felds 	
        	a=refresh(a,a.length,a[0].length);	//Updaten des Felds
        	try	//Verz�gerung um das Spielfeld besser zu sehen
        	{	
    			Thread.sleep(200);
    		} catch (InterruptedException e) 
        	{   			
    			e.printStackTrace();
    		}
        }
        //Falls das Spiel endet
        utf8P(a);	//Ausgabe des leeren Spielfelds
        System.out.print("Tot in "+counter+" Runden");	//Ausgabe der Rundenzahl
	}
	
	/** 
	*Gibt das Spielfeld Ziffer f�r Ziffer aus
	*/
	public static void print(int[][] a)	
	{
		for(int i=0;i<=a.length-1;i++)	//geht alle Zeilen durch
		{
			for(int j=0;j<=a[0].length-1;j++)	///geht alle Spalte durch
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println(" ");
		}
	}
	
	/** 
	*Gibt ein ganzes Spielfeld aufeinmal aus
	*Ausgabe mit Unicode Zeichen f�r bessere visualit�t
	*Bitte unter "run configurations" -> "common" UTF8 ausw�hlen
	*/
	public static void utf8P(int[][] a)	
	{										
        String s= "";	
    	for (int i=0;i<=a.length-1;i++) 
    	{
            for (int j=0;j<=a[0].length-1;j++) {
                if (a[i][j] == 0) {
                    s+="\u25A1 ";
                }
                else {
                	s+="\u25A0 ";
                }
            }
            s+=System.lineSeparator();	//neue Zeile            
        }
    	System.out.println(s);
    }
		
	/** 
	*Updatet das Spielfeld
	*/
	public static int[][] refresh(int[][] a, int h, int w)	 
	{		
        int[][] b = copy(a);		//Kopie vom �bergebenden Feld
        
        for(int i = 0; i < h; i++)	//Gehe alle Zeilen durch
        {
            for(int j = 0; j < w; j++)	//Gehe alle Spalten durch
            {
                if(a[i][j] == 1)	//Spielregel f�r lebende Zelle
                {
                    if (nighLiving(a, i, j) < 2 || nighLiving(a, i, j) > 3)	
                    {
                        b[i][j] = 0;	
                    }
                }
                
                if(a[i][j] == 0)	//Spielregel f�r tote Zelle
                {
                    if (nighLiving(a, i, j) == 3) 
                    {
                        b[i][j] = 1;
                    }
                }               
            }
        }
        return b;
    }
	
	/** 
	*Anzahl der lebenden Zellen in der N�he
	*/
	public static int nighLiving(int[][] a, int x, int y) 
	{		
        int living = 0;

        for (int i = x-1; i <= x+1; i++)	//alle Zeilen durchgehen
        {					
            for (int j = y-1; j <= y+1; j++)	//alle Spalten durchgehen
            {
            	if(i==x&&j==y)	//Nichts machen falls die zu �berpr�fende Zelle 
            	{}else 
            	{
            		living=living+a[modulo(i, a.length)][modulo(j, a[0].length)];	//Wert der Zelle addieren
            	}
            }
        }
        return living;
    }
	
	/** 
	*Gibt die Anzahl aller lebenden Zellen zur�ck
	*/
	public static int living(int[][] a) {	
        int living = 0;
        for(int i=0;i<a.length;i++)	//alle Zeilen 
        {
        	for(int j=0;j<a[i].length;j++)	//alle Spalten
        	{
					living+=a[i][j];	//Z�hlt Wert aller Zellen
        	}
        }
        return living;
    }
	
	/** 
	*Kopiert die Werte des ersten Arrays in das zweite
	*/
	public static int[][] copy(int[][] a)
	{
		int[][] b = new int [a.length][a[0].length];	//gleichgro�es Array erstellen
		for(int i=0;i<a.length;i++)	//Zeilen 
		{
			for(int j=0;j<a[0].length;j++)	//Spalten
			{
				b[i][j]=a[i][j];	//gleichsetzen
			}
		}
		return(b);
	}
	
	/** 
	*Generiert ein neues Spielfeld der Groesse l*b
	*/
	public static int[][] field(int l, int b)	
	{
		int a[][]=new int [l][b];	
		Random r=new Random();	//Generiert einen neuen random Generator
		int d;	//Variable f�r Wert der zu setztenden Zelle
		for(int x=0;x<=l-1;x++)	//Zeilen
		{
			for(int y=0;y<=b-1;y++)	//Spalten
			{
				d = r.nextInt(2);	//setzt d auf eine random Nummer zwischen 0 und 1
				a[x][y]=d;	
			}
		}		
		return a;
	}
	
	/** 
	*Errechnet das echte modulo
	*/
	public static int modulo(int a, int b) 
	{	
        if (a%b < 0) 
        {
             return(a+b);
        }
        return(a%b);
    }
}
