package �bung3;



public class GameOfLife {
    public static void main(String[] args) {
        String path ="D://Documents/Uni/AuP/conwayA.txt";
        int[][] a = util.FileUtils.readIntMatrix(path);
        //int[][] conwayA = FileUtils.readIntMatrix("D://Documents/Uni/AuP/conwayA.txt");

        int[][] t = {{0, 1, 0},
                     {1, 1, 1},
                     {1, 0, 1}};

        int[][] blinker = {{0, 0, 0, 0, 0},
                           {0, 0, 1, 0, 0},
                           {0, 0, 1, 0, 0},
                           {0, 0, 1, 0, 0},
                           {0, 0, 0, 0, 0}};
        int counter=0;
        while(getTotalLiving(a)>0) {
        counter++;
        PprintBoard(a);
        System.out.println(" ");		
        a=getNewBoard(a);
        try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
        }System.out.print(counter);
        
    }

    public static void _printBoard(int[][] board) {				//Hab ich
        for (int[] row : board) {
            for (int cell : row) {
                System.out.print(cell + " ");
            }
            System.out.println();
        }
    }

    public static void printBoard(int[][] board) {				//Hab ich
        for (int[] row : board) {
            for (int cell : row) {
                if (cell == 0) {
                    System.out.print(". ");
                }
                else {
                    System.out.print("o ");
                }
            }
            System.out.println();
        }
    }
    public static void PprintBoard(int[][] board) {				//Hab ich
        String s= "";
    	for (int[] row : board) {
            for (int cell : row) {
                if (cell == 0) {
                    s+="\u25A1 ";
                }
                else {
                	s+="\u25A0 ";
                }
            }s+=System.lineSeparator();
            
        }
    	System.out.println(s);
    }

    public static int countNeighbors(int[][] board, int row, int col) {		////Hab ich
        int height = board.length;
        int width = board[0].length;

        int count = 0;

        for (int i = row-1; i <= row+1; i++) {					
            for (int j = col-1; j <= col+1; j++) {
                if (i != row || j != col) {
                    count += board[mod(i, height)][mod(j, width)];
                }
            }
        }

        return count;
    }


    public static int mod(int a, int b) {					//Hab ich
        int result = a % b;
        if (result < 0) {
            result += b;
        }
        return result;
    }


    public static int[][] getNewBoard(int[][] oldBoard) {		//??????
        int height = oldBoard.length;
        int width = oldBoard[0].length;
        int[][] newBoard = getDeepCopy(oldBoard);		//Kopie vom zu �bergebenden Board

        int tmpNeighbors;

        for (int row = 0; row < height; row++) {					//?????
            for (int col = 0; col < width; col++) {
                tmpNeighbors = countNeighbors(oldBoard, row, col);

                // dead
                if (oldBoard[row][col] == 0) {
                    if (tmpNeighbors == 3) {
                        newBoard[row][col] = 1;
                    }
                }
                // alive
                else if (oldBoard[row][col] == 1) {
                    if (tmpNeighbors < 2 || tmpNeighbors > 3) {
                        newBoard[row][col] = 0;
                    }
                }
            }
        }

        return newBoard;
    }


    public static int getTotalLiving(int[][] board) {		//Lebende checken
        int total = 0;

        for (int[] row : board) {
            for (int cell : row) {
                total += cell;
            }
        }

        return total;
    }


    public static int[][] getDeepCopy(int[][] array) {		//Nur normales kopieren?
        int height = array.length;
        int width = array[0].length;

        int[][] copy = new int[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                copy[i][j] = array[i][j];
            }
        }

        return copy;
    }

    public static boolean identicalBoards (int[][] boardA, int[][] boardB) {	//easy
        int height = boardA.length;
        int width = boardA[0].length;

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if (boardA[row][col] != boardB[row][col]) {
                    return false;
                }
            }
        }

        return true;
    }
}
